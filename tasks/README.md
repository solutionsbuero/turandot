# Building Tasks

Create source distribution and wheel:

```bash
invoke distbuild
```

Create deb for `amd64`:

```bash
invoke builddeb --arch=amd64
```

Create deb for `armhf`:

```bash
invoke builddeb --arch=armhf
```

Create new deb package version (eg. package version 2):

```bash
invoke builddeb --pkgversion=2
```

Create Windows installer

```bash
invoke windowsinstaller
```

### Wheel sources

[Peewee Wheel](https://www.lfd.uci.edu/~gohlke/pythonlibs/#peewee)

### Software Sources

[GTK installer](https://github.com/tschoonj/GTK-for-Windows-Runtime-Environment-Installer)

## Notes

The Windows Story so far:

- The `exec_module` problem forces the use of Python3.10, which seems to completely avoid the issue.
- Weasyprint seems still dependent on GTK libs/dlls.
- The Python distribution that gets bundled with pynsist does NOT include tkinter, the [process of including it](https://pynsist.readthedocs.io/en/latest/faq.html?highlight=tkinter#packaging-with-tkinter) seems annoying
- The GTK installer for Windows seems actively maintained, relatively painless and includes the Adwaita theming assets
- pynsist should be able to include an [external installer](https://nsis.sourceforge.io/Embedding_other_installers) (as GTK) in its process, though this could prove hard to do reliably as it involves editing the installer.nsi file that is currently being built in the installer process. 

What now?

- There are basically two options now:
    - Continue to try to run tk as it looks better on Windows
    - Switch over to GTK as Weasyprint depends on it anyway => Doesn't work without MSYS2

### Powershell commands

_Activate venv:_

```powershell
Set-ExecutionPolicy -ExecutionPolicy Bypass -Scope Process -Force
.\venv\Scripts\Activate.ps1

deactivate
```
