import os
import platform
import shutil
from pathlib import Path
from invoke import task


own_dir = Path(__file__).parent.resolve()


"""
Helper Utilities
"""


def replace_placeholder_in_file(target: Path, old: str, new: str):
    """Replace place holders in text files in place"""
    with target.open('r+') as fh:
        txt = fh.read()
        txt = txt.replace(old, new)
        fh.seek(0)
        fh.write(txt)
        fh.truncate()


"""
Deb Utilities
"""


def get_deb_build_dir() -> Path:
    return Path("/tmp/turandotdeb")


def get_tmp_lib_dir():
    return Path("/tmp/turandotlibs")


def get_tmp_build_dir():
    return None


"""
Utility tasks
"""


@task
def distclean(c):
    """Completely clean dist folder"""
    os.chdir(own_dir.parent)
    for root, dirs, files in os.walk('./dist'):
        for f in files:
            os.unlink(os.path.join(root, f))
        for d in dirs:
            shutil.rmtree(os.path.join(root, d))


@task
def localinstall(c):
    """Install turandot in the local venv"""
    os.chdir(own_dir.parent)
    c.run("pip3 install .")


"""
PyPi tasks
"""


@task
def distbuild(c):
    """Build sdist and wheel to upload to PyPi"""
    os.chdir(own_dir.parent)
    c.run("python3 setup.py sdist bdist_wheel")


@task(pre=[distclean, distbuild])
def testpublish(c):
    """Publish package to temporary PyPi"""
    os.chdir(own_dir.parent)
    c.run("twine upload --repository testpypi dist/*")


@task(pre=[distclean, distbuild])
def publish(c):
    """Publish package to PyPi"""
    os.chdir(own_dir.parent)
    c.run("twine upload dist/*")


"""
Debian packaging tasks
"""


@task
def collectlibs(c):
    """Collect all dependencies as wheels or source packages"""
    # Packages without published wheels
    nonwhls = ['peewee', 'PyGObject', 'pycairo']
    # Set working dir
    os.chdir(own_dir)
    collectpath = get_tmp_lib_dir()
    if collectpath.is_dir():
        shutil.rmtree(collectpath)
    collectpath.mkdir()
    scriptpath = collectpath / "collectlibs.sh"
    shutil.copy("assets/scripts/collectlibs.sh", scriptpath)
    vertuple = platform.python_version_tuple()
    replace_placeholder_in_file(scriptpath, '{PYMAJ}', vertuple[0])
    replace_placeholder_in_file(scriptpath, '{PYMIN}', vertuple[1])
    replace_placeholder_in_file(scriptpath, '{VENVDIR}', str(collectpath))
    replace_placeholder_in_file(scriptpath, '{NONWHL}', " ".join(nonwhls))
    os.chdir(collectpath)
    c.run("bash collectlibs.sh".format(str(scriptpath)))
    shutil.rmtree(collectpath / "venv")
    scriptpath.unlink()


@task(pre=[localinstall, collectlibs])
def builddeb(c, arch="amd64", pkgversion="1"):
    """Build debian package from collected libs and template"""
    # Import components
    from turandot import __version__ as v
    from turandot.model import ModelUtils
    # Set working dir
    os.chdir(own_dir)
    # Determinate package name
    debname = "turandot_{}-{}_{}".format(v, pkgversion, arch)
    debfolder = own_dir.parent / "deb"
    debfile = debname + ".deb"
    print(debfolder / debfile)
    if (debfolder / debfile).is_file():
        raise FileExistsError("This package version has already been built")
    basepath = get_deb_build_dir() / debname
    if basepath.is_dir():
        shutil.rmtree(basepath)
    # Copy template structure
    shutil.copytree("assets/deb_template", basepath)
    libdir = basepath / "opt" / "turandot" / "libraries"
    for i in os.listdir(get_tmp_lib_dir()):
        shutil.copy(get_tmp_lib_dir() / i, libdir)
    # Copy icon to build folder
    with (basepath / "opt" / "turandot" / "turandot.svg").open("w") as fh:
        fh.write(ModelUtils.get_asset_content("turandot.svg"))
    # Adjust control files
    ctrlfile = basepath / "DEBIAN" / "control"
    vertuple = platform.python_version_tuple()
    replace_placeholder_in_file(ctrlfile, "{VER}", v)
    replace_placeholder_in_file(ctrlfile, "{PKGVER}", pkgversion)
    replace_placeholder_in_file(ctrlfile, "{ARCH}", arch)
    replace_placeholder_in_file(ctrlfile, '{PYMAJ}', vertuple[0])
    replace_placeholder_in_file(ctrlfile, '{PYMIN}', vertuple[1])
    postinst = basepath / "DEBIAN" / "postinst"
    replace_placeholder_in_file(postinst, '{PYMAJ}', vertuple[0])
    replace_placeholder_in_file(postinst, '{PYMIN}', vertuple[1])
    # Adjust permissions
    os.chmod(ctrlfile, 0o755)
    os.chmod(postinst, 0o755)
    os.chmod(basepath / "DEBIAN" / "postrm", 0o755)
    os.chmod(basepath / "DEBIAN" / "prerm", 0o755)
    os.chmod(basepath / "opt" / "turandot" / "turandot.sh", 0o755)
    # Build deb
    os.chdir(basepath.parent)
    c.run("dpkg-deb --build {}".format(debname))
    # Move deb to package dir
    debfolder.mkdir(exist_ok=True)
    (debfolder / debfile).unlink(missing_ok=True)
    shutil.move(basepath.parent / debfile, debfolder)


"""
Windows packaging tasks
"""


@task
def buildwheels(c):
    """Temporary deployment of a venv with all dependencies for the windows installation (to dump requirements)"""
    os.chdir(own_dir)
    # Deploy shell script
    shutil.copy("tmpinstallation.sh", "../dist/")
    os.chdir("../dist")
    # Edit shell script
    tmpinstall = Path("tmpinstallation.sh")
    vertuple = platform.python_version_tuple()
    replace_placeholder_in_file(tmpinstall, '{PYMAJ}', vertuple[0])
    replace_placeholder_in_file(tmpinstall, '{PYMIN}', vertuple[1])
    # Run shell script
    c.run("bash {}".format(str(tmpinstall)))
    # Build wheels for packages with none available
    os.chdir(own_dir)
    shutil.copy("buildwheels.sh", "/tmp/turandotbuild")
    os.chdir("/tmp/turandotbuild")
    print("BUILDING WHEELS NOW!")
    c.run("bash buildwheels.sh")
    print("FINISHED BUILDING WHEELS")


@task
def cleantmp(c):
    """Cleanup temporary deployment"""
    shutil.rmtree("/tmp/turandotbuild")


def build_dependency_string() -> str:
    """Build dependency string from temporarily deployed requirements file"""
    local_wheels = ["peewee", "qrcode", "tkhtmlview"]
    with Path("/tmp/turandotbuild/requirements.txt").open("r") as f:
        txt = f.read()
    dependencies = ""
    for i in txt.splitlines(keepends=False):
        pgkname = i.split("==")[0]
        if pgkname not in local_wheels:
            dependencies = "{}    {}\n".format(dependencies, i)
    return dependencies


@task(pre=[distclean, buildwheels, localinstall], post=[cleantmp])
def windowsinstaller(c):
    """Use pynsist to build exe installer"""
    os.chdir(own_dir)
    from turandot import __version__ as v
    # Deploy installer config
    shutil.copy("installer.cfg", "../dist/")
    # Copy local wheels
    shutil.copytree("./local_wheels", "../dist/local_wheels")
    # Create icon
    os.chdir(own_dir.parent)
    c.run("convert turandot/assets/turandot.png -background none -resize 128x128 -density 128x128 dist/turandot.ico")
    # Edit installer file
    os.chdir("./dist")
    dependencies = build_dependency_string()
    installercfg = Path("installer.cfg")
    replace_placeholder_in_file(installercfg, '{DEPENDENCIES}', dependencies)
    replace_placeholder_in_file(installercfg, '{VER}', v)
    replace_placeholder_in_file(installercfg, '{PYVER}', platform.python_version())
    # Copy-in newly exported wheels
    c.run("cp /tmp/turandotbuild/tkhtmlview* ./local_wheels")  # tkhtmlview wheel
    c.run("cp /tmp/turandotbuild/qrcode* ./local_wheels")  # qrcode wheel
    # Build installer
    c.run("pynsist installer.cfg")
