# Core dependencies
beautifulsoup4==4.11.2
Mako==1.2.4
markdown==3.5.2
python-frontmatter==1.0.0
requests==2.31.0
urllib3==2.2.1
weasyprint==56.0
cssselect2==0.7.0
jinja2==3.1.3
markdown-katex==202112.1034
colour==0.1.5
lxml==5.1.0
pyyaml==6.0.1
Pygments==2.17.2
ruamel.yaml==0.18.6
md_citeproc==0.2.2
sqlalchemy==2.0.27
markdown-captions==2.1.2

# Tk dependencies
tkhtmlview==0.2.0
bidict==0.23.1

# GTK dependencies
pygobject==3.46.0

# Optional templating dependencies
gitpython==3.1.42
qrcode==7.4.2
swissqr==0.2.0

# Dev dependencies
pytest==8.0.2
nose==1.3.7
mkdocs==1.5.3
mkdocstrings==0.24.1
twine==5.0.0
invoke==2.2.0
pynsist==2.8
