import unittest
import weasyprint
from weasyprint.text.fonts import FontConfiguration
import tempfile
from pathlib import Path


class TestWindowsWeasyprint(unittest.TestCase):

    TESTCASE_MARKUP = """
        <!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>Document</title>
        </head>
        <body>
            hello world
        </body>
        </html>
    """

    @classmethod
    def setUpClass(cls) -> None:
        pass

    @classmethod
    def tearDownClass(cls) -> None:
        pass

    def test_001_create_get_delete_assets(self):
        target_dir = Path(tempfile.gettempdir())
        print("Building pdf in {}".format(str(target_dir)))
        html = weasyprint.HTML(
            string=TestWindowsWeasyprint.TESTCASE_MARKUP
        )
        doc = html.render(font_config=FontConfiguration())
        doc.write_pdf(target=target_dir / "output.pdf")
